## Quotes-manager API
## Installation

- Clone this repo to directory on your server
- Run "composer install" and install all needed dependencies
- Rename .env.example file to .env and specify your database and email connection details
- Run migrations by "php artisan migrate"
- Generate app key by "php artisan key:generate"
- Run "php artisan migrate"
- Run "php artisan passport:install"
- Run "php artisan passport:client --personal"

## Postman Collection
https://restless-moon-6522.postman.co/collections/1094881-0552803a-11c7-76ba-20a3-15b8e4f31ff8?workspace=e3f9200c-1c9e-4254-8733-29de5734fa17#fd1d1aa5-acc7-4036-169b-b07a2ce6731f
