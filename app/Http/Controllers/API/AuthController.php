<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Notifications\RegisterActivate;

class AuthController extends Controller
{
    /**
     * Create new user
     *
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(StoreUserRequest $request)
    {
        try {
            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'activation_token' => str_random(60),
            ]);
            $user->save();
            $user->notify(new RegisterActivate($user));

            return response()->json([
                'message' => 'User was created! Check your email for activating account',
            ], 201);
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['status' => 'fail'], 500);
    }

    /**
     *
     * Registered user activation
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerActivate($token)
    {
        try {
            $user = User::where('activation_token', $token)->first();
            if (!$user) {
                return response()->json([
                    'message' => 'Activation token is invalid',
                ], 404);
            }

            $user->activate = true;
            $user->activation_token = '';
            $user->save();

            return $user;
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['status' => 'fail'], 500);
    }

    /**
     * User login and token creating
     *
     * @param LoginUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginUserRequest $request)
    {
        try {
            $credentials = request(['email', 'password']);
            $credentials['activate'] = 1;
            $credentials['deleted_at'] = null;
            if (!Auth::attempt($credentials)) {
                return response()->json([
                    'message' => 'Unathorized',
                ], 401);
            }
            $user = $request->user();
            $tokenResults = $user->createToken('Personal Access Token');
            $token = $tokenResults->token;
            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1);
            }
            $token->save();

            return response()->json([
                'access_token' => $tokenResults->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse($tokenResults->token->expires_at)->toDateTimeString(),
            ]);
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['error' => 'fail'], 500);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Logged out',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function delete(Request $request)
    {
        User::where('id', $request->user()->id)
            ->first()
            ->delete();

        return response()->json(null, 204);
    }
}
