<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\StoreCategoryRequest;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if ($request->input('perPage')) {
            $response = CategoryResource::collection(Category::where('user_id', $request->user()->id)->paginate($request->input('perPage')));
        } else {
            $response = CategoryResource::collection(Category::where('user_id', $request->user()->id)->get());
        }

        return $response;
    }

    /**
     * @param StoreCategoryRequest $request
     * @return CategoryResource
     */
    public function store(StoreCategoryRequest $request)
    {
        $category = Category::create([
            'user_id' => $request->user()->id,
            'name' => $request->name,
        ]);

        return new CategoryResource($category);
    }

    /**
     * @param Request $request
     * @param $category
     * @return CategoryResource
     */
    public function show(Request $request, $category)
    {
        $category = Category::where('user_id', $request->user()->id)->where('id', $category)->first();

        return new CategoryResource($category);
    }

    /**
     * @param StoreCategoryRequest $request
     * @param Category $category
     * @param $id
     * @return CategoryResource|\Illuminate\Http\JsonResponse
     */
    public function update(StoreCategoryRequest $request, $category)
    {
        $category = Category::where('user_id', $request->user()->id)->where('id', $category)->first();
        $category->update($request->only('name'));

        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $category)
    {
        $category = Category::where('user_id', $request->user()->id)->where('id', $category)->first();
        if (count($category->tasks)) {
            return response()->json([
                'error' => 'Can`t delete category while exists related task`s'
            ], 403);
        }
        $category->delete();

        return response()->json(null, 204);
    }
}
