<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Requests\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Task;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class TaskController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if ($request->input('perPage')) {
            $response = TaskResource::collection(Task::where('user_id',
                $request->user()->id)->paginate($request->input('perPage')));
        } else {
            $response = TaskResource::collection(Task::where('user_id', $request->user()->id)->get());
        }

        return $response;

    }

    /**
     * @param TaskRequest $request
     * @return TaskResource
     */
    public function store(TaskRequest $request)
    {
        $category = Category::findOrFail($request->category_id);
        $task = Task::create([
            'title' => $request->title,
            'description' => $request->description,
            'priority' => $request->priority,
            'user_id' => $request->user()->id,
            'category_id' => $category ? $request->category_id : null,
        ]);

        return new TaskResource($task);
    }

    /**
     * @param $id
     * @return TaskResource
     */
    public function show(Request $request, $task)
    {
        $task = Task::where('id', $task)->where('user_id', $request->user()->id)->get();

        return new TaskResource($task);
    }

    /**
     * @param TaskRequest $request
     * @param $task
     * @return TaskResource|\Illuminate\Http\JsonResponse
     */
    public function update(TaskRequest $request, $task)
    {
        $task = Task::where('id', $task)->where('user_id', $request->user()->id)->first();

        $task->update([
            'title' => $request->title,
            'description' => $request->description,
            'priority' => $request->priority,
            'category_id' => $request->category_id,
        ]);

        return new TaskResource($task);
    }

    /**
     * @param $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $task)
    {
        $task = Task::where('id', $task)->where('user_id', $request->user()->id);
        $task->delete();

        return response()->json(null, 204);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll(Request $request)
    {
        $task = Task::where('user_id', $request->user()->id)->get();
        $task->each->delete();

        return response()->json(null, 204);
    }

    public function search(Request $request)
    {
        try {
            $task = Task::where('user_id', $request->user()->id);

            if ($request->input('type') && $request->input('value') && $task) {
                switch ($request->input('type')) {
                    case 'title':
                        $result = $task->where('title', 'like', "%{$request->input('value')}%")->get();
                        break;
                    case 'date':
                        $result = $task->whereDate('created_at', Carbon::parse($request->input('value')))->get();
                        break;
                    case 'category':
                        $result = $task->where('category_id', $request->input('value'))->get();
                        break;
                    default:
                        $result = $task->get();
                        break;
                }

                return TaskResource::collection($result);
            } else {
                return response()->json([
                    'error' => 'Missing parameter',
                ], 422);
            }
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['error' => 'fail'], 500);
    }
}
