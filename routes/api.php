<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'API\AuthController@login');
    Route::post('register', 'API\AuthController@register');
    Route::get('register/activate/{token}', 'API\AuthController@registerActivate');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'API\AuthController@logout');
        Route::get('user', 'API\AuthController@user');
        Route::delete('delete', 'API\AuthController@delete');
    });
});
Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('categories', 'API\CategoryController');
    Route::apiResource('tasks', 'API\TaskController');
    Route::delete('tasks/deleteAll', 'API\TaskController@destroyAll');
    Route::post('tasks/search', 'API\TaskController@search');
});
